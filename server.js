const express = require('express');
const morgan = require('morgan');
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');

app.use(morgan(":method :url :status :res[content-length] - :response-time ms"));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
// Serve static files
app.use(express.static(path.join(__dirname, 'public')));



const pool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
});

//#region API
// Read All Characters
app.get("/characters", async (req, res) => {
  try {
    const [characters] = await pool.promise().query("SELECT * FROM characters");
    res.json(characters);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

// Read Single Character by ID
app.get("/characters/:id", async (req, res) => {
  try {
    const id = parseInt(req.params.id);
    const [character] = await pool.promise().query("SELECT * FROM characters WHERE id = ?", [id]);
    
    if (character.length > 0) {
      res.json(character[0]);
    } else {
      res.status(404).send("Character not found");
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
});

// Create a New Character
app.post("/characters", async (req, res) => {
  try {
   // const { name, age, description } = req.body;
   // if (!name || !age || !description) {
   //   return res.status(400).send("Name,age and description are required");
   // }
   const { name} = req.body;
    if (!name) {
      return res.status(400).send("Name are required");
    }

    const result = await pool.promise().query(
      //"INSERT INTO characters (name, age, description) VALUES (?, ?, ?)",
      //[name, age, description]
      "INSERT INTO characters (name) VALUES (?)",
      [name]
    );

    const newCharacterId = result[0].insertId;
    //res.status(201).json({ id: newCharacterId, name, age, description });
    res.status(201).json({ id: newCharacterId, name});
  } catch (error) {
    res.status(500).send(error.message);
  }
});

// Update Character
app.put("/characters/:id", async (req, res) => {
  try {
    const id = parseInt(req.params.id);
    //const { name, age, description } = req.body;
    const { name, description } = req.body;
    const result = await pool.promise().query(
     //"UPDATE characters SET name = ?, age = ?, description = ? WHERE id = ?",
      //[name, age, description, id]
      "UPDATE characters SET name = ? ,details=? WHERE id = ?",
      [name,description, id]
    );

    if (result[0].affectedRows > 0) {
      //res.status(200).json({ id, name, age, description });
      res.status(200).json({ id, name ,description });
    } else {
      res.status(404).send("Character not found");
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
});

// Delete Character
app.delete("/characters/:id", async (req, res) => {
  try {
    const id = parseInt(req.params.id);

    //const result = await pool.promise().query("DELETE FROM characters WHERE id = ?", [id]);
    const result = await pool.promise().query("SELECT * FROM characters WHERE id = ?", [id]);
    if (result[0].affectedRows > 0) {
      res.status(204).send(result);
    } else {
      res.status(404).send("Character not found");
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
});
//#endregion

//#region View-Action
app.get('/home', async (req, res) => {
  try {
    //const [characters] = await pool.promise().query("SELECT * FROM characters");
    //res.render('characters-page', { characters });
    res.render(path.join(__dirname, 'views', 'home'));

  } catch (error) {
    res.status(500).send(error.message);
  }
});

// Add a new character
app.post("/add-character", async (req, res) => {
  try {
    const { name } = req.body;
    if (!name) {
      //const { name, age, description } = req.body;
      //if (!name || !age || !description) {
      //return res.status(400).send("Name, age, and description are required");
      return res.status(400).send("Name are required");
    }

    await pool.promise().query(
      //"INSERT INTO characters (name, age, description) VALUES (?, ?, ?)",
      //[name, age, description]
      "INSERT INTO characters (name) VALUES (?)",
      [name]
    );

    res.redirect('/home');
  } catch (error) {
    res.status(500).send(error.message);
  }
});



// Update an existing character
app.post("/update-character/:id", async (req, res) => {
  try {
    const id = parseInt(req.params.id);
    //const { name, age, description } = req.body;
    const { name, description } = req.body;
    await pool.promise().query(
      //"UPDATE characters SET name = ?, age = ?, description = ? WHERE id = ?",
      "UPDATE characters SET name = ?, details = ? WHERE id = ?",
      [name, description, id]
    );

    // Fetch characters data again
    const [characters] = await pool.promise().query("SELECT * FROM characters");
    // Pass the notification message as a local variable
    res.locals.notification = 'Update successful!';
    
    // Render the characters-page template with the notification
    //res.render(path.join(__dirname, 'views', 'characters-page'), { notification: res.locals.notification },{ characters });
    res.render('home', { notification: res.locals.notification ,characters});
    //res.json({ notification: 'Update successful!' });
  } catch (error) {
    res.status(500).send(error.message);
  }
});


// Delete a character
app.post("/delete-character/:id", async (req, res) => {
  try {
    const id = parseInt(req.params.id);

    //await pool.promise().query("DELETE FROM characters WHERE id = ?", [id]);
    await pool.promise().query("SELECT * FROM characters WHERE id = ?", [id]);
     
    res.redirect('/home');
  } catch (error) {
    res.status(500).send(error.message);
  }
});
//#endregion

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));
